const express = require("express");
const router = express.Router();
const tmSchema = require("../models/Tm");
const { inputValidation } = require("../validation");

// GET a list of documents
router.get("/document", async (req, res) => {
  try {
    const results = await tmSchema.find();
    res.json(results);
  } catch (err) {
    res.json({ message: err });
  }
});

//GET Document by ID
router.get("/document/:id", async (req, res) => {
  // check if id exists
  const results = await tmSchema.findById(req.params.id);
  if (!results) return res.status(404).send("id not found");

  res.json(results);
});

//Filter based on Status
router.get("/status/:status", async (req, res) => {
  try {
    const results = await tmSchema.find({ status: req.params.status });
    res.json(results);
  } catch (err) {
    res.json({ message: err });
  }
});

// POST document
router.post("/", async (req, res) => {
  //validate input information

  const { error } = inputValidation(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // check exists
  const tmExist = await tmSchema.findOne({ tm: req.body.tm });
  if (tmExist) return res.status(400).send("tm already exists");

  const newData = new tmSchema({
    tm: req.body.tm,
    status: req.body.status,
    uploader: req.body.uploader,
    reviewer: req.body.reviewer
  });

  try {
    const savedData = await newData.save();
    res.json(savedData);
  } catch (err) {
    res.status(400).send(err);
  }
});

module.exports = router;
