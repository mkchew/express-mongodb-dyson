const mongoose = require("mongoose");

const tmSchema = new mongoose.Schema({
  tm: {
    type: String,
    required: true,
    max: 255
  },
  status: {
    type: String,
    required: true,
    max: 255
  },
  uploader: {
    type: String,
    required: true,
    max: 255
  },
  reviewer: {
    type: String,
    required: true,
    max: 255
  }
});

module.exports = mongoose.model("Schema", tmSchema);
